
#
# User configuration sourced by interactive shells
#

# Source zim
if [[ -s ${ZDOTDIR:-${HOME}}/.zim/init.zsh ]]; then
  source ${ZDOTDIR:-${HOME}}/.zim/init.zsh
fi

path=(
  /usr/local/{bin,sbin}
  /usr/local/opt/make/libexec/gnubin
  /usr/local/opt/coreutils/libexec/gnubin
  "$(yarn global bin)"
  "/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
  $path
)

manpath=(
  "/usr/local/opt/make/libexec/gnuman"
  "/usr/local/opt/coreutils/libexec/gnuman"
  $manpath
)

#
# Editors
#

export EDITOR=code
export REACT_EDITOR="$EDITOR"
export VISUAL="EDITOR"
export PAGER='less'

#
# Aliases
#

alias vim=nvim
alias vi=vim
alias tmux='tmux -2'
alias gpr="git pull-request -o"
alias git=hub
alias ls='ls --color'

export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
export FZF_DEFAULT_OPTS="--height 40% --border"
export HOMEBREW_GITHUB_API_TOKEN="34061971d7c01adeff34570de909f31cd723ecf6"

eval "$(fasd --init auto)"

[ -f ~/.fzf.zsh ] && . ~/.fzf.zsh
