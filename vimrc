call plug#begin('~/.vim/plugged')

Plug '/usr/local/opt/fzf' | Plug 'junegunn/fzf.vim'
Plug 'chriskempson/base16-vim'
Plug 'christoomey/vim-tmux-navigator'
Plug 'edkolev/tmuxline.vim'
Plug 'kshenoy/vim-signature'
Plug 'leafgarland/typescript-vim'
Plug 'mildred/vim-bufmru'
Plug 'mxw/vim-jsx'
Plug 'ntpeters/vim-better-whitespace'
Plug 'tomasiser/vim-code-dark'
Plug 'pangloss/vim-javascript'
Plug 'qpkorr/vim-bufkill'
Plug 'schickling/vim-bufonly'
Plug 'scrooloose/nerdcommenter'
Plug 'tmux-plugins/vim-tmux'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-ruby/vim-ruby'
Plug 'w0rp/ale'

" tpope!
Plug 'tpope/tpope-vim-abolish'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'

call plug#end()

let mapleader = ","

let g:ruby_path = []

inoremap jk <ESC>
nnoremap <Leader>t :FZF<CR>

set background=dark
set clipboard=unnamed
set colorcolumn=81
set expandtab
set foldcolumn=3
set formatoptions+=t
set formatprg=par\ -reqw80
set grepprg=rg\ --vimgrep
set incsearch
set laststatus=2
set mouse=a
set nocompatible
set nohlsearch
set noswapfile
set number
set omnifunc=syntaxcomplete#Complete
set scrolloff=5
set shiftround
set shiftwidth=2
set softtabstop=2
set tabstop=8
set termguicolors
set textwidth=80
set textwidth=80
set visualbell
set winwidth=86

set undofile
set backup
set undodir=~/.vim/.undo/
set backupdir=~/.vim/.backup/
set directory=~/.vim/.swp/

colorscheme codedark

let g:ale_lint_delay=2000
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'codedark'
let g:airline_powerline_fonts = 1
let g:jsx_ext_required = 0

let g:rails_projections = {
  \ "app/lib/*.rb": {
  \   "test": [
  \     "spec/lib/{}_spec.rb"
  \   ],
  \ }}

nnoremap <silent> <Leader><Leader>v :edit ~/.vimrc<cr>
nnoremap <silent> <leader><leader>u :edit ~/.tmux.conf<cr>
nnoremap <silent> <Leader>d :BD<cr>
nnoremap <silent> <Leader><Leader>` :source ~/.vimrc<CR>

let g:airline_mode_map = {
  \ '__' : '-',
  \ 'n'  : 'N',
  \ 'i'  : 'I',
  \ 'R'  : 'R',
  \ 'c'  : 'C',
  \ 'v'  : 'V',
  \ 'V'  : 'L',
  \ '' : 'B',
  \ 's'  : 'SELECT',
  \ 'S'  : 'S-LINE',
  \ '' : 'S-BLOCK',
  \ }

autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType ruby,eruby let g:rubycomplete_buffer_loading = 1 
autocmd FileType ruby,eruby let g:rubycomplete_classes_in_global = 1
autocmd FileType ruby,eruby let g:rubycomplete_rails = 1
autocmd FileType ruby compiler ruby

nnoremap [b :BufMRUPrev<CR>
nnoremap ]b :BufMRUNext<CR>

command! -bang -nargs=* Find call fzf#vim#grep('rg --column --line-number --no-heading --fixed-strings --ignore-case --no-ignore --hidden --follow --glob "!.git/*" --color "always" '.shellescape(<q-args>).'| tr -d "\017"', 1, <bang>0)

let test#strategy = 'dispatch'
nmap <silent> <leader>r :TestNearest<CR>
nmap <silent> <leader>R :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

let g:tmuxline_preset = 'nightly_fox'

let g:ale_fixers = {
\   'javascript': [
\       'eslint',
\       'prettier'
\   ],
\   'json': 'prettier',
\   'css': 'prettier',
\   'scss': 'prettier',
\   'md': 'prettier'
\}

let g:ale_fix_on_save = 1
let g:ale_javascript_prettier_use_local_config = 1

au BufNewFile,BufRead Dockerfile.* set filetype=Dockerfile
